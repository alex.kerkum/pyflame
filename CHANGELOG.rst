Version 0.2.1
-------------

* Fix exception if an -o/--output-file argument was specified on the command
  line.

Version 0.2.0
-------------

* Minor tweaks to flamegraph stack entry formatting.
* Fix incorrect IPython pyflame magic message when flamegraph.pl could not be found.
* Internal reformatting and cleanups.

Version 0.1.0
-------------

* Initial release.
